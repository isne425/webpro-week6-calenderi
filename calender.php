<html>
	<head>
		<meta charset="utf-8">
		<title>Calender</title>
		<link rel="stylesheet" type="text/css" href="table.css">
	</head>

	<body>

		<?php
			include 'function.php';	
		?>

		<center><h1>My Calendar</h1></center>


		<center><div class="calender">
			<div class="days">Sunday</div>
			<div class="days">Monday</div>
			<div class="days">Tuesday</div>
			<div class="days">Wednesday</div>
			<div class="days">Thursday</div>
			<div class="days">Friday</div>
			<div class="days">Saturday</div>
			<?php
				for($i = 1 ; $i <= $firstday ; $i++){
					echo '<div class="date2 blankday"> </div>';
				}
			?>

			<?php
				
				for($i=1; $i<=$days; $i++){
					echo '<div class="date';
					if ($today == $i && $todaymonth==$month && $todayyear == $year){
						echo ' today';
					}
					echo '">' . $i . '<br>';

					// mysql
					$mysqli = new mysqli("localhost", "root", "", "calendar");
					$qry = "SELECT * FROM events";
					$result = $mysqli->query($qry);

					while ($row = $result->fetch_array()){
						$dbday = date('d',strtotime($row['date']));
						$dbmonth = date('m',strtotime($row['date']));
						$dbyear = date('Y',strtotime($row['date']));
						$dbtitle = $row['title'];
			
						if($dbday==$i&&$dbmonth==$month&&$dbyear==$year){
							echo "<br><a href = 'detail.php'>". $dbtitle . "</a></br>" ;
							
						}
					}
					
					echo '</div>';
				}
			?>

			<?php
				$daysleft = 7 - (($days + $firstday)%7);
				if($daysleft<7){
					for($i=1; $i<=$daysleft; $i++){
						echo '<div class="date blankday"></div>';
					}
				}
			?>
			
		</div></center>

	</body>
	
</html>